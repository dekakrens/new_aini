import Arga from "@/assets/images/Arga.jpg";
import Deny from "@/assets/images/Deny.jpg";
import Dita from "@/assets/images/Dita.jpg";
import Edgar from "@/assets/images/Edgar.jpg";
import Kurnia from "@/assets/images/Kurnia.jpg";
import Nisa from "@/assets/images/Nisa.jpg";
import Oscar from "@/assets/images/Oscar.jpg";

export const teams = {
  hustler: [
    {
      name: "Muhammad Arga Ghifari",
      desc: "3 Years Experienced CEO at A Link Indonesia",
      image: Arga,
    },
    {
      name: "Nisa Elvia Wintami",
      desc: "2 Years Experienced as Entrepreneur",
      image: Nisa,
    },
  ],
  hipster: [
    {
      name: "Edgar Lous Deo Sumarsono",
      desc: "4 Years Experienced  as Product Designer for Startups",
      image: Edgar,
    },
    {
      name: "Oscar Alaska Samosir",
      desc: "5 Years Experienced IR-GA & Legal at Manufacturing Company ",
      image: Oscar,
    },
    {
      name: "Wahyu Kurniyati",
      desc: "4 Years Experienced Junior Assistant HCBP at Bank Rakyat Indonesia",
      image: Kurnia,
    },
  ],
  hacker: [
    {
      name: "Deny Kurniawan",
      desc: "1 Year Experienced as FE Engineer at Software House",
      image: Deny,
    },
    {
      name: "Naradita Kunti Nabila",
      desc: "2 Years Experienced Product Manager at Startup",
      image: Dita,
    },
  ],
};

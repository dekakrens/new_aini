const color = {
  primary700: "#ab1224",
  primary600: "#cd1a20",
  primary500: "#ee2d24", //logo
  primary400: "#f46d59",
  primary300: "#f99679",
  primary200: "#fdc1a7",
  primary100: "#fee3d2",
  primary50: "#fff9f9",
  accent700: "#6f1a9f",
  accent600: "#9226be",
  accent500: "#b934de",
  accent400: "#d764eb",
  accent300: "#ed84f5",
  accent200: "#fbaefb",
  accent100: "#fdd6fa",
  accent50: "#fff4fe",
  neutral700: "#1f1f1f",
  neutral600: "#4b4b4b",
  neutral500: "#8e8e8e",
  neutral400: "#cacaca",
  neutral300: "#e1e1e1",
  neutral200: "#eeeeee",
  neutral100: "#fafafa",
  neutral50: "#fafafa",
  black: "#000000",
  white: "#ffffff",
};

export const colors = {
  btn: {
    primary: color.accent400,
    secondary: color.white,
    tertiary: color.accent100,
  },
  btnText: {
    primary: color.white,
    secondary: color.accent300,
  },
  btnBorder: {
    secondary: color.accent300,
  },
  text: {
    primary: color.neutral700,
    secondary: color.neutral500,
    tertiary: color.accent400,
  },
  border: {
    primary: color.accent300,
    secondary: color.neutral400,
  },
  background: color.accent50,
  card: color.primary50,
};

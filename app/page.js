"use client";
import navItems from "@/assets/dummy/navigation.json";
import Logo from "@/assets/images/logo-aini.png";
import {
  ContactSection,
  FAQSection,
  HomeSection,
  SolutionsSection,
  TeamSection,
  TestimonialsSection,
} from "@/components";
import { colors } from "@/utils/colors";
import MenuIcon from "@mui/icons-material/Menu";
import {
  AppBar,
  Box,
  Button,
  Container,
  CssBaseline,
  Divider,
  Drawer,
  IconButton,
  List,
  ListItem,
  ListItemButton,
  ListItemText,
  Toolbar,
} from "@mui/material";
import Image from "next/image";
import { useState } from "react";

const drawerWidth = 240;

export default function Page(props) {
  const { window } = props;
  const [mobileOpen, setMobileOpen] = useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen((prevState) => !prevState);
  };

  const drawer = (
    <Box onClick={handleDrawerToggle} sx={{ textAlign: "center" }}>
      <Image src={Logo} alt="logo" height={40} width={100} objectFit="cover" />
      <Divider />
      <List>
        {navItems.map((item) => (
          <ListItem key={item.href} disablePadding>
            <ListItemButton sx={{ textAlign: "center" }}>
              <ListItemText primary={item.nav} />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
    </Box>
  );

  const container =
    window !== undefined ? () => window().document.body : undefined;

  return (
    <Box sx={{ display: "flex", background: colors.background }}>
      <CssBaseline />
      <AppBar component="nav" sx={styles.appBar}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            sx={styles.iconBtn}
          >
            <MenuIcon />
          </IconButton>
          <Box sx={styles.titleNav}>
            <Image
              src={Logo}
              alt="logo"
              height={40}
              width={100}
              objectFit="cover"
              sx={styles.titleNav}
            />
          </Box>
          <Box sx={{ display: { xs: "none", sm: "block" } }}>
            {navItems.map((item) => (
              <Button key={item.href} sx={styles.navBtn} href={item.href}>
                {item.nav}
              </Button>
            ))}
          </Box>
        </Toolbar>
      </AppBar>

      <nav>
        <Drawer
          container={container}
          variant="temporary"
          open={mobileOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
          sx={styles.drawer}
        >
          {drawer}
        </Drawer>
      </nav>

      <Container maxWidth="xl">
        <Toolbar />
        <HomeSection />
        <SolutionsSection />
        {/* <TestimonialsSection /> */}
        <FAQSection />
        <TeamSection />
        <ContactSection />
      </Container>
    </Box>
  );
}

const styles = {
  appBar: {
    backdropFilter: "saturate(180%) blur(20px)",
    backgroundColor: "transparent",
    boxShadow: "none",
  },
  iconBtn: { mr: 2, display: { sm: "none" } },
  titleNav: {
    flexGrow: 1,
  },
  drawer: {
    display: { xs: "block", sm: "none" },
    "& .MuiDrawer-paper": {
      boxSizing: "border-box",
      width: drawerWidth,
    },
  },
  navBtn: {
    color: colors.text.primary,
    fontWeight: "600",
  },
};

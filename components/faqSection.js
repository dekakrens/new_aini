"use cient";

import faqItems from "@/assets/dummy/faq.json";
import { colors } from "@/utils/colors";
import ExpandLess from "@mui/icons-material/ExpandLess";
import ExpandMore from "@mui/icons-material/ExpandMore";
import { Box, Container, Typography } from "@mui/material";
import Collapse from "@mui/material/Collapse";
import List from "@mui/material/List";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";
import { useState } from "react";
import { Section } from "./atoms/Section";

const Faq = () => {
  const [open, setOpen] = useState();

  const handleClick = (id) => {
    setOpen(id);
  };

  return (
    <Section id="faq">
      <Container md sx={styles.container}>
        <Typography variant="h5" sx={styles.sectionTitle}>
          FAQ
        </Typography>
        <List
          sx={{ width: "100%", bgcolor: "background.paper" }}
          component="nav"
        >
          {faqItems.map((item) => (
            <Box key={item.id}>
              <ListItemButton
                sx={styles.listButton}
                onClick={() => handleClick(item.id)}
              >
                <Typography sx={{ fontWeight: "600" }}>{item.q}</Typography>
                {open == item.id ? <ExpandLess /> : <ExpandMore />}
              </ListItemButton>

              <Collapse in={open == item.id} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>
                  <ListItemButton sx={{ pl: 4 }}>
                    <ListItemText primary={item.a} />
                  </ListItemButton>
                </List>
              </Collapse>
            </Box>
          ))}
        </List>
      </Container>
    </Section>
  );
};

export default Faq;

const styles = {
  listButton: { dispay: "flex", justifyContent: "space-between" },
  container: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column",
    height: "80vh",
  },
  sectionTitle: { fontWeight: "600", color: colors.text.primary, mb: 5 },
};

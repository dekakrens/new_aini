import { Box, Button, Grid, Typography } from "@mui/material";
import { Section } from "./atoms/Section";
import { colors } from "@/utils/colors";
import Image from "next/image";
import cover from "@/assets/images/cover.png";
import coverpng from "@/assets/images/cover-1.png";

const Home = () => {
  return (
    <Section id="home">
      <Grid container spacing={0} sx={styles.container}>
        <Grid item lg={6} sx={styles.leftSection}>
          <Typography variant="h1" sx={{ fontWeight: "600" }}>
            Unify, Automate, Elevate
          </Typography>
          <Typography variant="h5">
            Assistant for Integrated Solutions to Fit Your Business Demand
          </Typography>

          <Box>
            <Button
              variant="contained"
              href="https://www.figma.com/proto/lfzoZUWICfqa3S6xi8DyDZ/Design-Sprint-Kelompok-6?type=design&node-id=178-8901&t=snsK0oLMDbDmb11M-1&scaling=min-zoom&page-id=12%3A759&starting-point-node-id=17%3A1983"
              target="_blank"
              sx={[styles.button, { mr: 2 }]}
            >
              Try Our Solutions
            </Button>
            <Button variant="outlined" href="#contact" sx={styles.btnSecondary}>
              Contact Us
            </Button>
          </Box>
        </Grid>

        <Grid item lg={6} sx={styles.rightSection}>
          {/* Content */}
          <Image
            alt="cover"
            src={coverpng}
            height={750}
            width={850}
            objectFit="contain"
          />
        </Grid>
      </Grid>
    </Section>
  );
};

export default Home;

const styles = {
  container: {
    height: "100vh",
    position: "relative",
  },
  leftSection: {
    display: "flex",
    flexDirection: "column",
    flex: 1,
    justifyContent: "center",
    alignItems: "flex-start",
  },
  rightSection: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    height: "100%",
  },
  button: {
    mt: 4,
    mb: 2,
    borderRadius: 3,
    p: 2,
    textTransform: "none",
    backgroundColor: colors.btn.primary,
    ":hover": {
      backgroundColor: colors.btn.primary,
    },
  },
  btnSecondary: {
    mt: 4,
    mb: 2,
    borderRadius: 3,
    p: 2,
    textTransform: "none",
    color: colors.btnText.secondary,
    border: `1px solid ${colors.btnBorder.secondary}`,
    ":hover": {
      border: `1px solid ${colors.btnBorder.secondary}`,
    },
    fontWeight: "600",
  },
};

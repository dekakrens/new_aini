import React from "react";
import { Section } from "./atoms/Section";
import { Box, Typography } from "@mui/material";
import { colors } from "@/utils/colors";

const Soutions = () => {
  return (
    <Section id="solutions">
      <Box sx={styles.container}>
        <Typography variant="h5" sx={styles.sectionTitle}>
          Solution
        </Typography>

        <iframe
          src="https://www.figma.com/embed?embed_host=vercel&url=https://www.figma.com/proto/lfzoZUWICfqa3S6xi8DyDZ/Design-Sprint-Kelompok-6?type=design&node-id=17-1983&t=snsK0oLMDbDmb11M-1&scaling=min-zoom&page-id=12%3A759&starting-point-node-id=17%3A1983"
          height="100%"
          width="100%"
        />
      </Box>
    </Section>
  );
};

export default Soutions;

const styles = {
  container: {
    height: "100vh",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    pt: 10,
    // mb: 30,
  },
  sectionTitle: { fontWeight: "600", color: colors.text.primary, mb: 5 },
};

"use client";

import { teams } from "@/assets/dummy/team";
import { colors } from "@/utils/colors";
import Box from "@mui/material/Box";
import Tab from "@mui/material/Tab";
import Tabs from "@mui/material/Tabs";
import Typography from "@mui/material/Typography";
import Image from "next/image";
import { useState } from "react";
import { Section } from "./atoms/Section";

function CustomTabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <Box sx={styles.tabContainer}>{children}</Box>}
    </div>
  );
}

const CustomCard = ({ name, desc, image }) => {
  return (
    <Box sx={styles.card}>
      <Image src={image} alt={name} height="140" width="100%" />

      <Typography variant="h5" component="div" sx={{ mt: 3 }}>
        {name}
      </Typography>
      <Typography variant="body2" color={colors.text.secondary}>
        {desc}
      </Typography>
    </Box>
  );
};

const Team = () => {
  const [value, setValue] = useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <Section id="team">
      <Box sx={styles.container}>
        <Typography variant="h5" sx={styles.sectionTitle}>
          Our Team
        </Typography>

        <Tabs
          value={value}
          onChange={handleChange}
          centered
          textColor="secondary"
          indicatorColor="secondary"
        >
          <Tab label="Hustler" />
          <Tab label="Hipster" />
          <Tab label="Hacker" />
        </Tabs>

        <CustomTabPanel value={value} index={0}>
          {teams.hustler.map((item) => (
            <CustomCard
              key={item.name}
              name={item.name}
              desc={item.desc}
              image={item.image}
            />
          ))}
        </CustomTabPanel>

        <CustomTabPanel value={value} index={1}>
          {teams.hipster.map((item) => {
            return (
              <CustomCard
                key={item.name}
                name={item.name}
                desc={item.desc}
                image={item.image}
              />
            );
          })}
        </CustomTabPanel>

        <CustomTabPanel value={value} index={2}>
          {teams.hacker.map((item) => {
            return (
              <CustomCard
                key={item.name}
                name={item.name}
                desc={item.desc}
                image={item.image}
              />
            );
          })}
        </CustomTabPanel>
      </Box>
    </Section>
  );
};

export default Team;

const styles = {
  container: {
    height: "100vh",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    // pt: 10,
    // mb: 30,
  },
  tabContainer: {
    display: "flex",
    flexdirections: "row",
    overflow: "auto",
    justifyContent: "center",
    pt: 2,
  },
  card: {
    mx: 2,
    mb: 2,
    p: 2,
    width: 400,
    minHeight: 270,
    display: "flex",
    flexDirection: "column",
    borderRadius: 5,
    alignItems: "center",
    backgroundColor: colors.card,
    boxShadow: 5,
  },
  sectionTitle: { fontWeight: "600", color: colors.text.primary, mb: 5 },
};

import { colors } from "@/utils/colors";
import emailjs from "@emailjs/browser";
import styled from "@emotion/styled";
import MailOutlineIcon from "@mui/icons-material/MailOutline";
import WhatsAppIcon from "@mui/icons-material/WhatsApp";
import { Box, Button, Container, Grid, Typography } from "@mui/material";
import { useRef } from "react";
import { Section } from "./atoms/Section";

const Contact = () => {
  const form = useRef();

  const sendEmail = (e) => {
    e.preventDefault();

    emailjs
      .sendForm(
        "service_79qbf0l",
        "template_28pasbv",
        form.current,
        "AsJImNy9B8Fhqxz-Q"
      )
      .then(
        (res) => {
          console.log(res.text);
        },
        (error) => {
          console.log(error.text);
        }
      );

    e.target.reset();
  };

  return (
    <Section id="contact">
      <Container md sx={styles.container}>
        <Typography variant="h5" sx={styles.sectionTitle}>
          Contact Us
        </Typography>

        <Grid container spacing={4}>
          <Grid item lg={4} xs={12}>
            <Box sx={styles.leftContainer}>
              <Box sx={styles.btnContact}>
                <MailOutlineIcon />
                <Typography sx={styles.mediaTitle}>Email</Typography>
                <Typography>aini@gmail.com</Typography>
                <Anchor href="mailto:dekakrens@gmail.com" target="_bank">
                  Send a message
                </Anchor>
              </Box>

              <Box sx={styles.btnContact}>
                <WhatsAppIcon />
                <Typography sx={styles.mediaTitle}>Whatsapp</Typography>
                <Typography>+628529081xxxx</Typography>
                <Anchor
                  href="https://api.whatsapp.com/send?phone=+6285290815170"
                  target="_blank"
                >
                  Send a message
                </Anchor>
              </Box>
            </Box>
          </Grid>

          <Grid item lg={8} xs={12}>
            <Form ref={form} onSubmit={sendEmail}>
              <Input
                type="text"
                name="name"
                placeholder="Your Full Name"
                required
              />
              <Input
                type="email"
                name="email"
                placeholder="Your Email"
                required
              />
              <Input
                name="message"
                rows={"10"}
                placeholder="Your Message"
                required
              ></Input>

              <Button sx={styles.btnSend} type="submit">
                Send Message
              </Button>
            </Form>
          </Grid>
        </Grid>
      </Container>
    </Section>
  );
};

export default Contact;

const Form = styled("form")({
  display: "flex",
  flexDirection: "column",
  gap: "1.2rem",
});

const Input = styled("input")({
  width: "100%",
  padding: "1.5rem",
  borderRadius: "0.5rem",
  background: "transparent",
  border: `2px solid ${colors.border.primary}`,
  resize: "none",
  color: colors.text.primary,
});

const Anchor = styled("a")({
  textDecoration: "none",
  marginTop: "1rem",
});

const styles = {
  container: {
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    mb: 35,
  },
  sectionTitle: { fontWeight: "600", color: colors.text.primary, mb: 5 },
  leftContainer: { display: "flex", flexDirection: "column", gap: "1.2rem" },
  mediaTitle: { fontWeight: "600", mt: 1 },
  btnContact: {
    border: "none",
    borderRadius: 5,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    py: 2,
    backgroundColor: colors.btn.tertiary,
  },
  btnSend: {
    backgroundColor: colors.btn.primary,
    width: 170,
    textTransform: "none",
    color: colors.btnText.primary,
    borderRadius: 2,
    height: 48,
  },
};

import ContactSection from "./contactSection";
import FAQSection from "./faqSection";
import HomeSection from "./homeSection";
import SolutionsSection from "./solutionSection";
import TeamSection from "./teamSection";
import TestimonialsSection from "./testimoniasSection";

export {
  ContactSection,
  FAQSection,
  HomeSection,
  SolutionsSection,
  TeamSection,
  TestimonialsSection,
};
